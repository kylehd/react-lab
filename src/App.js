import React, { Component } from 'react';
import './App.css';

import ListComponent from './list/ListComponent';
import ColorSliderApp from "./color-slider/ColorSliderApp";

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>

        <ListComponent/>

        <ColorSliderApp/>
      </div>
    );
  }
}

export default App;
