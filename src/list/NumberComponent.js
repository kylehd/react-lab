
import React, { Component } from 'react';

export default class NumberComponent extends Component {

    render() {
        const numberStyle = {
            color: 'red'
        };

        return (
            <div style={numberStyle}>{this.props.value}</div>
        )
    }

}