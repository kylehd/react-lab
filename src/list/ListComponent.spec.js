
import React from 'react';
import {shallow} from 'enzyme';
import ListComponent from './ListComponent';
import NumberComponent from "./NumberComponent";

test('it should contain 5 NumberComponents', () => {
    const list = shallow(<ListComponent/>);
    expect(list.find(NumberComponent).length).toEqual(5);
});