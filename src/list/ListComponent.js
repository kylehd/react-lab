
import React, {Component} from 'react';
import NumberComponent from './NumberComponent';

export default class ListComponent extends Component {

    constructor() {
        super();
        this.state = {
            listItems: ["1", "2", "3", "4", "5"]
        }
    }

    render() {
        return (
            <div>
                <ul>
                    {this.state.listItems.map((item, i) => {
                        return <li key={i}><NumberComponent value={item}/></li>
                    })}
                </ul>
            </div>
        )
    }

}