
import React from 'react';
import {shallow } from 'enzyme';

import ColorDisplay from './ColorDisplay';
import Color from './Color';

test('ColorDisplay should contain a Color component for displaying a color', () => {
    const display = shallow(<ColorDisplay/>);
    expect(display.find(Color).length).toEqual(1);
});