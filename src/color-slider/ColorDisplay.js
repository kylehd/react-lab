import React, { Component } from 'react';

import Color from './Color';

export default class ColorDisplay extends Component {

    render() {
        return (
            <div>
                <Color color={this.props.color}/>
            </div>
        )
    }

}