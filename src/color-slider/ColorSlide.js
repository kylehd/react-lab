import React, { Component } from 'react';

import Color from './Color';

export default class ColorSlide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            colors: ['red', 'orange', 'yellow', 'green', 'blue', 'violet']
        };

        this.handleMouseMove = this.handleMouseMove.bind(this);
        this.handleMouseDown = this.handleMouseDown.bind(this);
        this.handleMouseUp = this.handleMouseUp.bind(this);

    }

    handleMouseMove(newColor) {
        this.props.onMouseMove(newColor);
    }

    handleMouseDown() {
        this.props.onMouseDown();
    }

    handleMouseUp() {
        this.props.onMouseUp();
    }

    render() {
        const sliderStyles = {
            display: 'flex',
            justifyContent: 'center'
        };

        return (
            <div style={sliderStyles}>
                {this.state.colors.map((item, i) => {
                   return <Color key={i} color={item} down={this.props.down} onMouseMove={this.handleMouseMove} onMouseDown={this.handleMouseDown} onMouseUp={this.handleMouseUp}/>
                })}
            </div>
        )
    }
}