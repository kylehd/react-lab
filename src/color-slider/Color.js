
import React, {Component} from 'react';

export default class Color extends Component {

    constructor(props) {
        super(props);

        this.handleMouseMove = this.handleMouseMove.bind(this);
        this.handleMouseDown = this.handleMouseDown.bind(this);
        this.handleMouseUp = this.handleMouseUp.bind(this);
    }

    handleMouseMove(e) {
        if (e.target && e.target.classList.contains('color') && this.props.down) {
            this.props.onMouseMove(this.props.color);
        }
    }

    handleMouseDown() {
        this.props.onMouseDown();
    }

    handleMouseUp() {
        this.props.onMouseUp();
    }

    render() {
        const colorStyles = {
            width: 175,
            height: 250,
            backgroundColor: this.props.color,
            color: 'white'
        };

        return (
            <div>
                <div className="color" style={colorStyles} onMouseMove={this.handleMouseMove} down={this.props.down} onMouseDown={this.handleMouseDown} onMouseUp={this.handleMouseUp}></div>
            </div>
        )
    }

}