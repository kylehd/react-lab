
import React from 'react';
import {shallow} from 'enzyme';
import ColorSlide from "./ColorSlide";
import Color from './Color';

test('ColorSlider should contain 6 Color components', () => {
    const slider = shallow(<ColorSlide/>);
    expect(slider.find(Color).length).toEqual(6)
});

