import React, { Component } from 'react';

import ColorDisplay from './ColorDisplay';
import ColorSlide from './ColorSlide';

export default class ColorSliderApp extends Component {

    constructor() {
        super();
        this.state = {
            currentColor: 'black',
            down: false
        };

        this.handleMouseMove = this.handleMouseMove.bind(this);
        this.handleMouseDown = this.handleMouseDown.bind(this);
        this.handleMouseUp = this.handleMouseUp.bind(this);
    }

    handleMouseMove(newColor) {
        if (this.state.down) {
            this.setState({
                currentColor: newColor,
                down: this.state.down
            });
        }
    }

    handleMouseDown() {
        this.setState({
            currentColor: this.state.currentColor,
            down: true
        });
    }

    handleMouseUp() {
        this.setState({
            currentColor: this.state.currentColor,
            down: false
        });
    }

    render() {
        return (
            <div>
                <ColorDisplay color={this.state.currentColor}/>
                <ColorSlide onMouseMove={this.handleMouseMove} down={this.state.down} onMouseDown={this.handleMouseDown} onMouseUp={this.handleMouseUp}/>
            </div>
        )
    }

}